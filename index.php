<?php

require_once 'vendor/autoload.php';

// membuat Slim variable bernama $log dan
// mendefinisikan beberapa konfigurasi
// seperti path posts, pagination
// dan markdown processor 'md'

$log = new Slim(array(
	'view'			=> new TwigView(),
	'posts.path'	=> "./posts",
	'md'			=> new 'dflydev\markdown\MarkdownParser()',
	'pagination'	=> 4
));

// Fungsi helper
//

$getMarkdownPosts = function ($dir) {
	// mengidentifikasi direktori iterator
	$files = new DirectoryIterator($dir);

	// membuat array yang berisi posts
	$posts = array();

	// looping ke DirectoryIterator
	foreach ($files as $posts) {
		// hanya memproses file berekstensi .md
		if($posts->getExtension() == 'md') {
			// get post files data
			$post_data = file_get_contents($post->getPathname());

			// mengambil konten 
			$post_data = explode('--', $post_data);

			// memindahkan konten data ke array dan di kembalikan jadi json
			$post_meta_json = array_shift($post_data);
			$post_meta = json_decode($post_meta_json, true);

			// parsing markdown ke HTML
			$md = new dflydev\markdown\MarkdownParser();
			$post_html = $md->transformMarkdown($post_data[0]);

			// menyimpan tiap post ke dalam array dan menambahkan
			// info post ke hash baru
			$posts[strtotime($post_meta['date'])] = array(
				'title'		=> $post_meta['title'],
				'desc'		=> $post_meta['desc'],
				'date'		=> $post_meta['date'],
				'tags'		=> $post_meta['tags'],
				'link'		=> $post->getBasename('.md'),
				'html'		=> $post_html
			);
		}  
	}

	// mengurutkan post berdasarkan tanggal 
	krsort($posts);

	// dikembalikan 
	return $posts;
};

// Route
//
$log->get('/', function () use ($log, $getMarkdownPosts) {
	//get posts
	$posts = $getMarkdownPosts($log->config('posts.path'));

	//limit post pagination
	$posts_limit = array_slice($postss, 0, $log->config('pagination'));

	// get total posts
	$posts_count = count($posts);

	// kalkulasi jumlah halaman
	$pages_number = ceil($posts_count / $log->config('pagination'));

	// render halaman utama
	$log->render('index.html', array(
		'posts'			=> $posts_limit,
		'page_current'	=> 1,
		'page_count'	=> $pages_number
	));
});

// halaman about
$log->get('/about', function () use ($log) {
	// render halaman about
	$log->render('about.html');
});

// halaman project
$log->get('/projects', function () use ($log) {
	// render halaman projects
	$log->render('projects.html');
});

// redirect log/page1 ke index root
$log->get('/log/1', function () use ($log) {
	$log->redirect('/');
});

// blog pagination
$log->get('/log/:number', function ($number) use ($log, $getMarkdownPosts) {
	// get posts
	$posts = $getMarkdownPosts($log->config('posts.path'));

	// menetukan posisi array
	$previous_page = $number - 1;
	$start_index = $previous_page * $log->config('pagination');

	// limit posts ke pagination
	$posts_limit = array_slice($post, $start_index, $log->config('pagination'));

	// get jumlah total posts
	$posts_count = count($posts);

	// kalkulasi total posts
	$pages_number = ceil($posts_count / $log->config('pagination'));

	// menentukan jika request terlalu tinggi di arahkan ke 401
	if($number > $pages_number) {
		$log->notFound();
	}

	$log->render('index.html', array(
		'posts'			=> $posts_limit,
		'page_current'	=> $number,
		'page_count'	=> $pages_number
	));
})->conditions(array('number' => '\d+'));

// blog index 
$log->get('/log', function () use ($log) {
	$log->redirect('/');
});

// post view
$log->get('/log/:post', function ($post) use ($log) {
	// get nama post dan direktori
	$dir = $log->config('posts.path');
	$post = $post . '.md';
	$post_path = $dir . '/' . $post;

	// redirect ke 404 jika request ke post tidak ada
	if (!file_exists($post_path)) {
		$log->notFound();
	}

	// get konten post
	$post_data = file_get_contents($post_path);

	// parsing kontent file post 
	$post_data = explode('--', $post_data);

	// mmengubah info data ke array dan di decode menjadi json
	$post_meta_json = array_shift($post_data);
	$post_meta = json_decode($post_meta_json, true);

	// mengubah markdown menjadi HTML
	$post_html = $log->config('md')->transformMarkdown($post_data[0]);

	// get informasi tentang request dan mengubah ke url panjang
	$request = $log->request();
	$headers = $request->headers();
	$resource_uri = $request->getResourceUri();

	// disqus
	full_url = ($headers['SERVER_PORT'] == '443') ? 'https://' : 'http://';
	full_url .= $headers['HOST'];
	full_url .= $resource_uri;

	// final
	$post_result = array(
		'title'		=> $post_meta['title'],
		'date'		=> $post_meta['date'],
		'tags'		=> $post_meta['tags'],
		'desc'		=> $post_meta['desc'],
		'html'		=> $post_html,
		'disqusid'	=> strtotime($post_meta['date']),
		'disqusurl'	=>	$full_url
	);

	// render post view
	$log->render('post.html', array('post' => $post_result));
});

// tag 
$log->get('/log/tagged/:tag', function ($tag) use ($log, $getMarkdownPosts) {
	// get semua posts
	$posts = $getMarkdownPosts($log->config('posts.path'));

	// pakai array untuk tag posts
	$tagged_posts = array();

	// mengurutkan posts
	foreach ($posts as $post) {
		// jika tag di post
		// push ke array baru
		if (in_array($tag, $post['tags'])) {
			array_push($tagged_posts, $post);
		}
	}

	// render tagged view
	$log->render('tagged.html', array(
		'tag'	=> $tag,
		'posts'	=> $tagged_posts
	));
});

// 404
$log->notFound(function () use ($log) {
	$log->render('404.html')
});

// Lest finish this
$log->run();
?>